import { Currency, ETHER, Token } from '@koingfu.com/sdk-bch'

export function currencyId(currency: Currency): string {
  if (currency === ETHER) return 'BCH'
  if (currency instanceof Token) return currency.address
  throw new Error('invalid currency')
}
